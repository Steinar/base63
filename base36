#! /usr/bin/env python

# Copyright 2022 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

import getopt, string, sys

HELP_TEXT = """\
base36 [-d] [-h] [-n]

-d
    Decode instead of encode.
-h
    Print this text and exit.
-n
    Do not append a newline at the end of the output.

Escape data read from stdin and output to stdout using a scheme only containing
[0-9A-Z]."""

DIGITS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
BASE = 36
assert len(DIGITS) == BASE

def base36(number):
    buffer = []
    running = number
    while running >= BASE:
        running, ordinal = divmod(running, BASE)
        buffer.append(DIGITS[ordinal])
    buffer.append(DIGITS[running])
    buffer.reverse()
    return ''.join(buffer)

# len(base36(0x10fffd)) == 4
# => 5, 6, 7, 8, 9 are used as special case prefixes chosen based on the
#    author's preference.
def encode(x):
    buffer = []
    for i in range(len(x)):
        c = x[i]
        if c in string.ascii_uppercase + string.digits:
            buffer.append("0" + c)
        elif c == " ":
            buffer.append("5")
        elif c == ",":
            buffer.append("6")
        elif c == ".":
            buffer.append("7")
        elif c == "(":
            buffer.append("8")
        elif c == ")":
            buffer.append("9")
        elif c in string.ascii_lowercase:
            buffer.append(c.upper())
        else:
            quoted = base36(ord(c))
            buffer.append(str(len(quoted)) + quoted)
    return "".join(buffer)

def from_base36(representation):
    sum = 0
    for c in representation:
        sum *= BASE
        sum += DIGITS.index(c)
    return sum

def check_base36(raw, offset, length):
    i = offset
    barrier = offset + length
    while i < barrier:
        if raw[i] not in DIGITS:
            raise ValueError("Illegal base36 digit at offset " + str(i) + ": "
                    + repr(raw[i]))
        i += 1
    return raw[offset:offset + length]

def decode(encoded):
    buffer = []
    i = 0
    while i < len(encoded):
        c = encoded[i]
        if c == "0":
            i += 1
            c = encoded[i]
            if c not in DIGITS:
                raise ValueError("Illegal input character at offset " + str(i)
                        + ": " + repr(encoded[i]))
            buffer.append(c)
        elif c in "1234":
            length = ord(c) - ord('0')
            ordinal = from_base36(check_base36(encoded, i + 1, length))
            buffer.append(chr(ordinal))
            i += length
        elif c == "5":
            buffer.append(" ")
        elif c == "6":
            buffer.append(",")
        elif c == "7":
            buffer.append(".")
        elif c == "8":
            buffer.append("(")
        elif c == "9":
            buffer.append(")")
        elif c in string.ascii_uppercase:
            buffer.append(c.lower())
        else:
            raise ValueError("Illegal input character at offset " + str(i)
                    + ": " + repr(encoded[i]))
        i += 1
    return "".join(buffer)

def main():
    options, arguments = getopt.gnu_getopt(sys.argv[1:], "dhn")
    do_decode = False
    newline = "\n"
    for name, value in options:
        if name == "-h":
            print(HELP_TEXT)
            sys.exit(0)
        elif name == "-d":
            do_decode = True
        elif name == "-n":
            newline = ''
    if len(arguments) != 0:
        print(HELP_TEXT, file=sys.stderr)
        sys.exit(1)
    if do_decode:
        print(decode(sys.stdin.read(-1)), end=newline)
    else:
        print(encode(sys.stdin.read(-1)), end=newline)

if __name__ == "__main__":
    main()
